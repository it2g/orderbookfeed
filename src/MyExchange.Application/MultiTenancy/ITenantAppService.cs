﻿using Abp.Application.Services;
using MyExchange.MultiTenancy.Dto;

namespace MyExchange.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

