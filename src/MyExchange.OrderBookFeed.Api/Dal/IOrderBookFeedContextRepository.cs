﻿using System;
using Abp.Domain.Repositories;
using MyExchange.OrderBookFeed.Api.Entity;

namespace MyExchange.OrderBookFeed.Api.Dal
{
    public interface IOrderBookFeedContextRepository : IRepository<OrderBookFeedContext, Guid>
    {
    }
}
