﻿using System;

namespace MyExchange.OrderBookFeed.Api.Dto
{
    public class CreateOrderBookFeedDto
    {
        public Guid PairId { get; set; }

        public string ExchangeId { get; set; }

        ///Задержка между запросами к бирже 
        public int Pause { get; set; } = 3000;

        /// Временное приращение/подрезание паузы
        public int PauseDelta { get; set; } = 100;
    }
}
