﻿using System;
using MyExchange.Api.Dto._DtoBase;

namespace MyExchange.OrderBookFeed.Api.Dto
{
    public class OrderBookFeedDto : BotTacticContextBaseDto
    {
        public Guid PairId { get; set; }

        public string ExchangeId { get; set; }

        ///Задержка между запросами к бирже 
        public int Pause { get; set; } = 3000;

        /// Временное приращение/подрезание паузы
        public int PauseDelta { get; set; } = 100;
    }
}
