﻿namespace MyExchange.OrderBookFeed.Api.Dto
{
    public class UpdateOrderBookFeedDto
    {
        ///Задержка между запросами к бирже 
        public int Pause { get; set; } = 3000;

        /// Временное приращение/подрезание паузы
        public int PauseDelta { get; set; } = 100;
    }
}
