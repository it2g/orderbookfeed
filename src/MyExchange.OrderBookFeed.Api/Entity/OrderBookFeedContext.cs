﻿using MyExchange.Api.BotTactic;

namespace MyExchange.OrderBookFeed.Api.Entity
{
    public class OrderBookFeedContext : BotTacticContextBase
    {
        public string PairIds { get; set; }

        public string ExchangeId { get; set; }

        ///Задержка между запросами к бирже 
        public int Pause { get; set; } = 3000;

        /// Временное приращение/подрезание паузы
        public int PauseDelta { get; set; } = 100;
    }
}
