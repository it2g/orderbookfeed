﻿using Abp.Application.Services;
using MyExchange.OrderBookFeed.Api.Entity;

namespace MyExchange.OrderBookFeed.Api
{
    public interface IOrderBookFeedBuilder : IApplicationService
    {
        IOrderBookFeed Create(OrderBookFeedContext context);
    }
}
