﻿using MyExchange.Api.BotTactic;
using MyExchange.OrderBookFeed.Api.Entity;

namespace MyExchange.OrderBookFeed.Api
{
    public interface IOrderBookFeedManager : IBotManager<IOrderBookFeed, OrderBookFeedContext>
    {
    }
}
