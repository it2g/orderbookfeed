﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using MyExchange.Api.Dto.Order;

namespace MyExchange.OrderBookFeed.Api.Services.Crud
{
    public interface IOrderBookFeedCrudAppService : IApplicationService
    {
        Task<IEnumerable<OrderDto>> GetOrderBookAsync(string exchangeId, string pairId, uint count, DateTime? date);
    }
}
