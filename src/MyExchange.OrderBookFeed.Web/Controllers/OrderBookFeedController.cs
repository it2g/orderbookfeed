﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.BackgroundJobs;
using Hangfire.Annotations;
using Microsoft.AspNetCore.Mvc;
using MyExchange.Api.Dto.Order;
using MyExchange.Controllers;
using MyExchange.OrderBookFeed.Api.BackgroundJobs;
using MyExchange.OrderBookFeed.Api.Services.Crud;

namespace MyExchange.OrderBookFeed.Web.Controllers
{
    [Route("api/[controller]")]
    public class OrderBookFeedController : MyExchangeControllerBase
    {
        private IBackgroundJobManager _backgroundJobManager;
        private readonly IOrderBookFeedCrudAppService _orderBookService;

        public OrderBookFeedController([NotNull] IBackgroundJobManager backgroundJobManager,
            [NotNull] IOrderBookFeedCrudAppService orderBookService)
        {
            _backgroundJobManager = backgroundJobManager ?? throw new ArgumentNullException(nameof(backgroundJobManager));
            _orderBookService = orderBookService ?? throw new ArgumentNullException(nameof(orderBookService));
        }

        /// <summary>
        /// Мониторинг сделок по определенному ордеру
        /// на указанной бирже
        /// TODO Создан для тестирования. Убрать после настройки запуска фоновых задач при
        /// запуске системы.
        /// </summary>
        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult> RunOrdersMonitoring([FromBody] OrderMonitoringInput value)
        {
            await _backgroundJobManager.EnqueueAsync<OrderBookFeedJob, (string, string)>((value.ExchangeId, value.PairId));

            return Ok();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="exchangeId"></param>
        /// <param name="pairId"></param>
        /// <param name="count">количество ордеров в книге</param>
        /// <param name="date">дата и время от которой нужно отсчитывать колисество ордеров</param>
        /// <returns></returns>
        [HttpGet]
        [Route("[action]")]
        public async Task<IEnumerable<OrderDto>> OrderBook(OrderBookInput input)
        {
            return await _orderBookService.GetOrderBookAsync(input.ExchangeId, input.PairId, input.Count, input.Date);
        }

        public class OrderBookInput
        {
            public string ExchangeId { get; set; }
            public string PairId { get; set; }
            public uint Count { get; set; }
            public DateTime? Date { get; set; }
        }
        public class OrderMonitoringInput
        {
            public string ExchangeId { get; set; }

            public string PairId { get; set; }
        }
    }
}
