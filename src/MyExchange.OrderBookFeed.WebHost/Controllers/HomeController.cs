﻿using Microsoft.AspNetCore.Mvc;
using MyExchange.Controllers;

namespace MyExchange.OrderBookFeed.WebHost.Controllers
{
    public class HomeController : MyExchangeControllerBase
    {
        public HomeController()
        {
        }

        public IActionResult Index() => Redirect("/swagger");
    }
}
