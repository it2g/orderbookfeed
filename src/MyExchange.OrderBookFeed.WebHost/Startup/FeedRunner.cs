﻿using System;
using System.Diagnostics;
using System.Linq;
using Microsoft.Extensions.Hosting;
using System.Threading;
using System.Threading.Tasks;
using Abp.Collections.Extensions;
using JetBrains.Annotations;
using MyExchange.Api.Constants;
using MyExchange.Api.Dto.PairInfoOnExchange;
using MyExchange.Api.Exchange;
using MyExchange.Api.Services.Crud;
using MyExchange.OrderBookFeed.Api;
using MyExchange.OrderBookFeed.Api.Dal;
using MyExchange.OrderBookFeed.Api.Entity;

namespace MyExchange.OrderBookFeed.WebHost
{
    /// <summary>
    /// Фоновый процесс запуска, получения стакана цен по зарегистрированным биржам и всем валютным парам
    /// </summary>
    public class FeedRunner : BackgroundService
    {
        private readonly IExchangeProvider _exchangeProvider;
        private readonly IPairInfoOnExchangeCrudAppService _pairInfoOnExchangeCrudAppService;
        private readonly IOrderBookFeedContextRepository _feedTacticContextRepository;
        private readonly IOrderBookFeedBuilder _feedTacticBuilder;
        private readonly IOrderBookFeedManager _feedTacticManager;
        private readonly object _locker = new object();

        public FeedRunner([NotNull] IExchangeProvider exchangeProvider,
            [NotNull] IPairInfoOnExchangeCrudAppService pairInfoOnExchangeCrudAppService,
            [NotNull] IOrderBookFeedContextRepository feedTacticContextRepository,
            [NotNull] IOrderBookFeedBuilder feedTacticBuilder,
            [NotNull] IOrderBookFeedManager tradeFeedTacticManager)
        {
            _exchangeProvider = exchangeProvider ?? throw new ArgumentNullException(nameof(exchangeProvider));
            _pairInfoOnExchangeCrudAppService = pairInfoOnExchangeCrudAppService ?? throw new ArgumentNullException(nameof(pairInfoOnExchangeCrudAppService));
            _feedTacticContextRepository = feedTacticContextRepository ?? throw new ArgumentNullException(nameof(feedTacticContextRepository));
            _feedTacticBuilder = feedTacticBuilder ?? throw new ArgumentNullException(nameof(feedTacticBuilder));
            _feedTacticManager = tradeFeedTacticManager ?? throw new ArgumentNullException(nameof(tradeFeedTacticManager));
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var exchanges = _exchangeProvider.GetAll();

            foreach (var exchange in exchanges)
            {
                if (exchange.Id == "ExmoExchange")
                {
                    var pairInfoOnExchangeList = await _pairInfoOnExchangeCrudAppService.GetAll(
                        new PairInfoOnExchangePagedResultRequestDto { ExchangeId = exchange.Id });

                    RunFeed(exchange.Id, pairInfoOnExchangeList.Items
                        .Take(10)//TODO убрать, когда будет решена проблема с преодалением лимита обращений на биржу
                        .Select(p => p.PairId)
                        .JoinAsString(";")
                    );
                }
            }
        }

        void RunFeed(string exchangeId, string pairIds)
        {
            OrderBookFeedContext orderTacticContext;
            lock (_locker)
            {
                orderTacticContext = _feedTacticContextRepository
                    .FirstOrDefault(x => x.ExchangeId == exchangeId
                                         && x.PairIds == pairIds
                                         && x.Status == BotStatuses.Work);
            }

            if (orderTacticContext == null)
            {
                orderTacticContext = new OrderBookFeedContext
                {
                    Pause = 10000,
                    PauseDelta = 200,
                    PairIds = pairIds,
                    ExchangeId = exchangeId
                };
            }
            else
            {
                Debug.WriteLine("Поднят из БД контекст мониторинга");
            }

            var botTactic = _feedTacticBuilder.Create(orderTacticContext);

            lock (_locker)
            {
                _feedTacticManager.Start(botTactic);
            }
        }
    }
}
