﻿using Abp.Hangfire;
using Abp.Hangfire.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using MyExchange.BitcoinExchange;
using MyExchange.ExchangeExmo;
using MyExchange.ExchangeHuobiPro;

namespace MyExchange.OrderBookFeed.WebHost
{
    [DependsOn(typeof(AbpHangfireAspNetCoreModule),
               typeof(MyExchangeWebCoreModule),
               typeof(MyExchangeMainModule),
               typeof(OrderBookFeedModule),
               typeof(ExmoExchangeModule),
               typeof(HuobiProExchangeModule),
               typeof(BitcoinExchangeModule)
    )]
    public class OrderBookFeedWebHostModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.BackgroundJobs.UseHangfire();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(OrderBookFeedWebHostModule).GetAssembly());
        }

        public override void PostInitialize()
        {
        }
    }
}
