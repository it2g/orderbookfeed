﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace MyExchange.OrderBookFeed.WebHost
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            var builder = Microsoft.AspNetCore.WebHost.CreateDefaultBuilder()
                .UseKestrel()
                .UseUrls("http://0.0.0.0:22025") //https://github.com/dotnet/core/issues/948  http://take.ms/VaudN
                .UseStartup<Startup>()
                .ConfigureServices(services =>
            {
                //Запускается фоновый процесс получения фида стакана цен
                services.AddHostedService<FeedRunner>();
            });

            return builder;
        }
    }
}
