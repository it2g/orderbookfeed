﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyExchange.Api.Dal.EfConfigurations;
using MyExchange.OrderBookFeed.Api.Entity;

namespace MyExchange.OrderBookFeed.Dal.EfConfigurations
{
    public class OrderBookFeedContextMap : BotTacticsContextBaseMap<OrderBookFeedContext>
    {
        public override void Configure(EntityTypeBuilder<OrderBookFeedContext> builder)
        {
            base.Configure(builder);

            builder.Property(x => x.ExchangeId).IsRequired().HasColumnName("exchange_id");
            builder.Property(u => u.PairIds).IsRequired().HasColumnName("pair_ids");
            builder.Property(u => u.Pause).IsRequired().HasColumnName("pause");
            builder.Property(u => u.PauseDelta).IsRequired().HasColumnName("pause_delta");

            builder.ToTable("order_book_feed_context", "feed");
        }
    }
}
