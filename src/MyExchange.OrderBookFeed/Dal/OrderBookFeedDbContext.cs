﻿using Abp.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MyExchange.Api.Entity;
using MyExchange.Api.Exchange;
using MyExchange.OrderBookFeed.Api.Entity;
using MyExchange.OrderBookFeed.Dal.EfConfigurations;

namespace MyExchange.OrderBookFeed.Dal
{
    public class OrderBookFeedDbContext : AbpDbContext
    {
        public virtual DbSet<OrderBookFeedContext> MonitoringBotTacticContexts { get; set; }

        public OrderBookFeedDbContext(DbContextOptions<OrderBookFeedDbContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //https://www.npgsql.org/efcore/value-generation.html - для автогенерации Id типа Guid
            modelBuilder.HasPostgresExtension("uuid-ossp");

            modelBuilder.ApplyConfiguration(new OrderBookFeedContextMap());

            modelBuilder.Ignore<Pair>();
            modelBuilder.Ignore<Currency>();
            modelBuilder.Ignore<Exchange>();
        }
    }
}
