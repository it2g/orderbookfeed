﻿using System.Data.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyExchange.OrderBookFeed.Dal
{
    public static class OrderBookFeedDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<OrderBookFeedDbContext> builder, string connectionString)
        {
            builder.UseNpgsql(connectionString,
                    //https://stackoverflow.com/questions/38507861/entity-framework-core-change-schema-of-efmigrationshistory-table
                    x => x.MigrationsHistoryTable(HistoryRepository.DefaultTableName, "feed"))
                .UseLazyLoadingProxies();
        }

        public static void Configure(DbContextOptionsBuilder<OrderBookFeedDbContext> builder, DbConnection connection)
        {
            builder.UseNpgsql(connection,
                    //https://stackoverflow.com/questions/38507861/entity-framework-core-change-schema-of-efmigrationshistory-table
                    x => x.MigrationsHistoryTable(HistoryRepository.DefaultTableName, "feed"))
                .UseLazyLoadingProxies();
        }
    }
}
