﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using MyExchange.Configuration;
using MyExchange.Web;

namespace MyExchange.OrderBookFeed.Dal
{
    public class OrderBookFeedDbContextFactory : IDesignTimeDbContextFactory<OrderBookFeedDbContext>
    {
        public OrderBookFeedDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<OrderBookFeedDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder(),
                environmentName: Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT"));

            OrderBookFeedDbContextConfigurer.Configure(builder, configuration.GetConnectionString(MyExchangeConsts.ConnectionStringName));

            return new OrderBookFeedDbContext(builder.Options);
        }
    }
}
