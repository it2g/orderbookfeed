﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Abp.Collections.Extensions;
using Abp.EntityFrameworkCore;
using Abp.EntityFrameworkCore.Repositories;
using Microsoft.EntityFrameworkCore;
using MyExchange.Api.Entity;
using MyExchange.OrderBookFeed.Api.Dal;
using MyExchange.OrderBookFeed.Api.Entity;


namespace MyExchange.OrderBookFeed.Dal.Repositories
{
    public class OrderBookFeedContextRepository :
        EfCoreRepositoryBase<OrderBookFeedDbContext, OrderBookFeedContext, Guid>,
        IOrderBookFeedContextRepository
    {
        public OrderBookFeedContextRepository(IDbContextProvider<OrderBookFeedDbContext> dbContextProvider) : base(
            dbContextProvider)
        {
        }

        public override OrderBookFeedContext Insert(OrderBookFeedContext entity)
        {
            entity = base.Insert(entity);

            return entity;
        }
    }
}
