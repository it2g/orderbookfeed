﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyExchange.OrderBookFeed.Migrations
{
    public partial class RenameFieldInFeedContext : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "pair_id",
                schema: "feed",
                table: "order_book_feed_context",
                newName: "pair_ids");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "pair_ids",
                schema: "feed",
                table: "order_book_feed_context",
                newName: "pair_id");
        }
    }
}
