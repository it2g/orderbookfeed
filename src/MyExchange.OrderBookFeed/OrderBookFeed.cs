﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Abp.AutoMapper;
using Abp.Domain.Uow;
using JetBrains.Annotations;
using MyExchange.Api.Dal;
using MyExchange.Api.Dto.Order;
using MyExchange.Api.Dto.Pair;
using MyExchange.Api.Dto.Trade;
using MyExchange.Api.Entity;
using MyExchange.Api.Exchange;
using MyExchange.Api.Services.Crud;
using MyExchange.Base.BotTactic;
using MyExchange.OrderBookFeed.Api;
using MyExchange.OrderBookFeed.Api.Entity;

namespace MyExchange.OrderBookFeed
{
    /// <summary>
    /// Стакан цен по валютным парам
    /// </summary>
    public class OrderBookFeed : BotTacticBase<OrderBookFeedContext>, IOrderBookFeed
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IExchangeProvider _exchangeProvider;
        private readonly IPairCrudAppService _pairCrudAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private static readonly object _lockerPairRead = new object();
        private static readonly object _lockerOrderSave = new object();
        private static readonly object _lockerExchangeOrderLoad = new object();

        public OrderBookFeed(
            [NotNull] IExchangeProvider exchangeProvider,
            [NotNull] IPairCrudAppService pairCrudAppService,
            [NotNull] IUnitOfWorkManager unitOfWorkManager,
            [NotNull] IOrderRepository orderRepository)
        {
            _exchangeProvider = exchangeProvider ?? throw new ArgumentNullException(nameof(exchangeProvider));
            _pairCrudAppService = pairCrudAppService ?? throw new ArgumentNullException(nameof(pairCrudAppService));
            _unitOfWorkManager = unitOfWorkManager ?? throw new ArgumentNullException(nameof(unitOfWorkManager));
            _orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
        }
        public override string Name { get; } = "Стакан цен";

        public override Action Execute => Feed;

        public override void Dispose() { }

        /// <summary>
        /// Собирает информацию о курсах валютных пар и сохраняет ее в БД для последующей аналитики
        /// </summary>
        [UnitOfWork(IsDisabled = false)]
        private async void Feed()
        {
            //процесс получения фида проходит первый цикл импорта данных
            //в таком случае необходимо исключить из общего объема данных уже ранее загруженные сделки
            bool isFirstTimeRun = true;

            //Сделки загруженные на предыдущем запросе
            var previousOrders = new Dictionary<PairDto, IEnumerable<OrderDto>>();

            var exchange = _exchangeProvider.Get(Context.ExchangeId);

            IList<PairDto> pairs = new List<PairDto>();

            var pairIdList = Context.PairIds.Split(";");

            foreach (var pairId in pairIdList)
            {
                lock (_lockerPairRead)
                {
                    var pair = _pairCrudAppService.Get(pairId).Result;
                    pairs.Add(pair);
                }
            }

            ///В цикле с установленной задержкой делать запросы на биржу по совершаемым
            /// сделкам отслеживаемого ордера.
            do
            {
                var loadedOrders = new Dictionary<PairDto, IEnumerable<OrderDto>>();

                foreach (var pair in pairs)
                {
                    lock (_lockerExchangeOrderLoad)
                    {
                        var orderList = exchange.ExchangeDriver.OrderBookAsync(pair).Result;
                        loadedOrders.Add(pair, orderList);
                    }
                    Debug.WriteLine(
                        $"Поток {Thread.CurrentThread.ManagedThreadId} закончил читать  данные с биржи");
                }

                if (loadedOrders.Count() == 0) break;
                
                if (isFirstTimeRun)
                {
                    //Должен исполняться только один раз при мсполнении первой итеррации цикла
                    //чтобы исключить дублирования загрузки
                    foreach (var keyValue in loadedOrders)
                    {
                        using (var uow = _unitOfWorkManager.Begin())
                        {
                            var loadedOrdersByPair = _orderRepository.GetAll()
                                .Where(t => t.ExchangeId == Context.ExchangeId && t.PairId == keyValue.Key.Id)
                                .OrderBy(t => t.CreationTime)
                                .Take(keyValue.Value.Count())
                                .ToList();

                            previousOrders.Add(keyValue.Key, loadedOrdersByPair.Select(t => t.MapTo<OrderDto>()));
                        }
                    }
                    isFirstTimeRun = false;
                }
                ///Оставляем только новые сделки, т.к. в последнем запросе естественно
                /// будут сделки, котрые были уже получены в предпоследнем запросе
                var lastNewOrders = GetOnlyNewOrders(loadedOrders, previousOrders);

                var ordersToWrite = lastNewOrders.Select(t => t.MapTo<Order>()).ToList();
                
                var countWritedTrades = await _orderRepository.InsertAsync(ordersToWrite);
               

                //Изменение задержки перед последующей загрузкой на основе анализа
                //повторно скаченных данных
                ChangePause(countWritedTrades, loadedOrders.Count());
#if DEBUG
                Debug.WriteLine($"Количество новых ордеров: {countWritedTrades} с биржи: {exchange.BriefName} Задержка: {Context.Pause} милисек");
#endif
                //сохраняем последние ордера, чтобы сравнить с ними вновь загружаемые ордера на 
                //следующем итеррации цикла
                previousOrders = loadedOrders;

                await Task.Delay(Context.Pause);

                //Ведется контроль за выставленным состоянием бота и в случае необходимости
                //прекращает мониторинг
                //https://docs.microsoft.com/ru-ru/dotnet/standard/parallel-programming/task-cancellation
                if (Context.CancellationTokenSource.IsCancellationRequested)
                {
                    ///TODO Предварительные операции по завершению работы мониторинга

                    Context.CancellationTokenSource.Token.ThrowIfCancellationRequested();
                }

            } while (true);

        }

        /// <summary>
        /// Вносит коррективы в величину паузы между двумя загрузками данных
        /// в зависимости от того, насколько быстро меняются данные.
        /// Настоящая реализация возможно несколько грубо отражает возможную динамику
        /// Этот  алгоритм можно уточнять и делать более диференцированый и более динамичный
        /// </summary>
        /// <param name="countNewOrders"></param>
        /// <param name="countOrders"></param>
        private void ChangePause(int countNewOrders, int countOrders)
        {
            if (countOrders == 0) return;

            if (countNewOrders / countOrders < 0.4)
            {
                //Увеличиваем задержку в 2 раза
                Context.Pause *= 2;
            }
            else if (countNewOrders / countOrders < 0.6)
            {
                //Увеличиваем задержку на дельту
                Context.Pause += Context.PauseDelta;
            }
            else if (countNewOrders / countOrders > 0.8)//80% всех загруженных сделок - это новые сделки
            {
                //Уменьшаем задержку на дельту
                Context.Pause -= Context.PauseDelta;
            }
            else if (countNewOrders / countOrders > 0.9)//90% всех загруженных сделок - это новые сделки
            {
                //Уменьшаем задержку в 2 раза
                Context.Pause /= 2;
            }
        }

        /// <summary>
        /// Возвращает новые ордера, не загруженые на предыдущих циклах
        /// </summary>
        /// <param name="loadedOrders"></param>
        /// <param name="previousOrders"></param>
        /// <returns></returns>
        private IList<OrderDto> GetOnlyNewOrders(IDictionary<PairDto, IEnumerable<OrderDto>> loadedOrders, 
                                                IDictionary<PairDto, IEnumerable<OrderDto>> previousOrders)
        {
            var allLoadedOrders = loadedOrders.SelectMany(x => x.Value);
            
            var allPreviousOrders = previousOrders.SelectMany(x => x.Value);
            
            var newOrders = allLoadedOrders.Except(allPreviousOrders, new OrderComparer()).ToList();


            return newOrders;
        }

        /// <summary>
        /// Сравниватель двух ордеров, загруженных из биржи
        /// </summary>
        class OrderComparer : IEqualityComparer<OrderDto>
        {
            public bool Equals(OrderDto x, OrderDto y)
            {
                return x.OrderId.Equals(y.OrderId);
            }

            public int GetHashCode(OrderDto obj)
            {
                return obj.OrderId.GetHashCode();
            }
        }
    }
}
