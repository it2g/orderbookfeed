﻿using Abp.Application.Services;
using Abp.Dependency;
using MyExchange.OrderBookFeed.Api;
using MyExchange.OrderBookFeed.Api.Entity;

namespace MyExchange.OrderBookFeed
{
    public class OrderBookFeedBuilder : ApplicationService, IOrderBookFeedBuilder
    {
        public IOrderBookFeed Create(OrderBookFeedContext context)
        {
            var botTactic = IocManager.Instance.Resolve<IOrderBookFeed>();

            botTactic.Context = context;

            return botTactic;
        }
    }
}
