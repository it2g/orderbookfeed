﻿using MyExchange.OrderBookFeed.Api.Dto;
using MyExchange.OrderBookFeed.Api.Entity;

namespace MyExchange.OrderBookFeed
{
    public class OrderBookFeedContextAutoMapperProfile : AutoMapper.Profile
    {
        public OrderBookFeedContextAutoMapperProfile()
        {
            CreateMap<OrderBookFeedDto, OrderBookFeedContext>().ReverseMap();
            CreateMap<CreateOrderBookFeedDto, OrderBookFeedDto>();
            CreateMap<UpdateOrderBookFeedDto, OrderBookFeedDto>();
        }
    }
}
