﻿using Abp.BackgroundJobs;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Exchange;
using MyExchange.Base.Services;
using MyExchange.OrderBookFeed.Api;
using MyExchange.OrderBookFeed.Api.Dal;
using MyExchange.OrderBookFeed.Api.Entity;

namespace MyExchange.OrderBookFeed
{
    public class OrderBookFeedManager : TradingTacticManagerBase<IOrderBookFeed, OrderBookFeedContext>
        , IOrderBookFeedManager
    {
        public OrderBookFeedManager(IBotTacticProvider botTacticProvider,
            IExchangeProvider exchangeProvider,
            IBackgroundJobManager backgroundJobManager,
            IOrderBookFeedContextRepository contextRepository)
            : base(botTacticProvider, exchangeProvider, backgroundJobManager, contextRepository)
        {
        }
    }
}
