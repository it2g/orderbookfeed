﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Hangfire.Annotations;
using MyExchange.Api.Dto.Order;
using MyExchange.Api.Services.Crud;
using MyExchange.OrderBookFeed.Api.Services.Crud;

namespace MyExchange.OrderBookFeed.Services.Crud
{
    public class OrderBookFeedCrudAppService : IOrderBookFeedCrudAppService
    {
        private readonly IOrderCrudAppService _orderCrudAppService;

        public OrderBookFeedCrudAppService([NotNull] IOrderCrudAppService orderCrudAppService)
        {
            _orderCrudAppService = orderCrudAppService ?? throw new ArgumentNullException(nameof(orderCrudAppService));
        }

        public async Task<IEnumerable<OrderDto>> GetOrderBookAsync(string exchangeId, string pairId, uint count, DateTime? date)
        {
            var result = (await _orderCrudAppService.GetAll(new OrderPagedResultRequestDto
            {

            })).Items;

            return result;
        }
    }
}
