﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using MyExchange.Api.Services.Crud;

namespace MyExchange.OrderBookFeed.Services.Crud
{
    public class OrderBookFeedCrudServiceBase<TEntity, TEntityDto, TGetAllInput, TPrimaryKey> :
        AsyncCrudAppService<TEntity, TEntityDto, TPrimaryKey, TGetAllInput, TEntityDto, TEntityDto, TEntityDto, TEntityDto>,
        IExhchangeCrudAppServices<TEntityDto, TPrimaryKey, TGetAllInput, TEntityDto, TEntityDto, TEntityDto, TEntityDto>
        where TEntity : class, IEntity<TPrimaryKey>
        where TEntityDto : IEntityDto<TPrimaryKey>, new()
    {
        public OrderBookFeedCrudServiceBase(IRepository<TEntity, TPrimaryKey> repository) : base(repository)
        {
        }
        public virtual async Task<TEntityDto> Get(TPrimaryKey id)
        {
            return await Get(new TEntityDto() { Id = id });
        }
        public virtual async Task Delete(TPrimaryKey id)
        {
            await Delete(new TEntityDto() { Id = id });
        }
    }
}
